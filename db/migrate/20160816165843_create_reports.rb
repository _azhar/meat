class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.datetime :timestamp
      t.string :time
      t.integer :mutton_id

      t.timestamps null: false
    end
  end
end
