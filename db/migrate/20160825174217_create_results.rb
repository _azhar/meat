class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :total_karkas
      t.integer :total_meat

      t.timestamps null: false
    end
  end
end
