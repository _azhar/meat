class AddPriceKgToProperty < ActiveRecord::Migration
  def change
    add_column :properties, :price_per_kg, :integer
  end
end
