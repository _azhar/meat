class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :property_id
      t.integer :total_kg
      t.integer :order_id

      t.timestamps null: false
    end
  end
end
