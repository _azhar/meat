class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :email
      t.string :name
      t.string :status, default: 'pending'
      t.string :phone_number
      t.text :address

      t.timestamps null: false
    end
  end
end
