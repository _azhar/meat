class CreateMuttons < ActiveRecord::Migration
  def change
    create_table :muttons do |t|
      t.integer :property_id
      t.integer :total_kg
      t.integer :total_box
      t.integer :report_id

      t.timestamps null: false
    end
  end
end
