class Property < ActiveRecord::Base
  validates :name, :code, :parent, presence: true
end
