class Report < ActiveRecord::Base

  validates :timestamp, presence: true

  belongs_to :result

  has_many :muttons
  accepts_nested_attributes_for :result
  accepts_nested_attributes_for :muttons, :allow_destroy => true

  scope :find_by_daily, -> daily { where(timestamp: Time.zone.now.beginning_of_day..Time.zone.now) if daily.present? }

  def total_rowspan
    self.muttons.count
  end

  def total_kg
    total = 0
    self.muttons.each do |mutton|
      total += mutton.total_kg.to_i
    end
    total
  end

  def total_box
    total = 0
    self.muttons.each do |mutton|
      total += mutton.total_box.to_i
    end
    total
  end

end
