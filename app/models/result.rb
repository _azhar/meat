class Result < ActiveRecord::Base
  validates :total_karkas, :total_meat, presence: true
end
