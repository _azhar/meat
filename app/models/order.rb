class Order < ActiveRecord::Base
  validates :phone_number, :email, :name, :address, presence: true

  has_many :items
  accepts_nested_attributes_for :items, :allow_destroy => true
end
