class Admin::PropertiesController < Admin::AdminController
  before_action :set_property, only: [:show, :edit, :update, :destroy]

  def index
    @properties = Property.all
  end

  def show
  end

  def new
    @property = Property.new
  end

  def edit
  end

  def update
    respond_to do |format|
      if @property.update(property_params)
        format.html { redirect_to admin_properties_path, notice: 'Master Data berhasil diubah' }
        format.json { render :show, status: :ok, location: @property }
      else
        format.html { render :edit }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to admin_properties_path, notice: 'Master Data berhasil dihapus' }
      format.json { head :no_content }
    end
  end

  def create
    @property = Property.new(property_params)
    respond_to do |format|
      if @property.save
        format.html { redirect_to admin_properties_path, notice: 'Master Data berhasil dibuat' }
        format.json { render :show, status: :created, location: @property }
      else
        format.html { render :new }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def set_property
      @property = Property.find(params[:id])
    end

    def property_params
      params.require(:property).permit(:name, :desc, :parent, :code, :price_per_kg)
    end

end
