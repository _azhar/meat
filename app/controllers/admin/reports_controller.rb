class Admin::ReportsController < Admin::AdminController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  PER_PAGE = 20

  def index
    @reports = Report.includes(:muttons).page(params[:page]).per(PER_PAGE).order(id: :desc)
    @reports = @reports.find_by_daily(params[:time_scope])
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "reoprts-#{Time.now.strftime('%Y-%m-%d')}",
               orientation: 'Landscape'
      end
    end
  end

  def show
  end

  def edit
  end

  def new
    @report = Report.new
    @result = @report.build_result
    Property.all.each do |property|
      @report.muttons.build(property_id: property.id)
    end
  end

  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to admin_reports_path, notice: 'Laporan berhasil dihapus.' }
    end
  end

  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to admin_reports_path, notice: 'Laporan berhasil diubah.' }
      else
        format.html { render :edit }
      end
    end
  end

  def create
    @report = Report.new(report_params)
    respond_to do |format|
      if @report.save
        format.html { redirect_to admin_reports_path, notice: 'Laporan berhasil dibuat.' }
      else
        @result = @report.build_result
        format.html { render :new }
      end
    end
  end

  private
    def set_report
      @report = Report.find(params[:id])
    end

    def report_params
      params.require(:report).permit(
        :timestamp,
        :time,
        :result_attributes => [:total_meat, :total_karkas],
        muttons_attributes: [
          :id,
          :total_box,
          :total_kg,
          :property_id
          ]
        )
    end

end
