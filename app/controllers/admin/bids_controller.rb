class Admin::BidsController < Admin::AdminController
  before_action :set_order, only: [:edit, :update, :destroy]
  PER_PAGE = 20

  def index
    @bids = Order.includes(:items).page(params[:page]).per(PER_PAGE).order(id: :desc)
    respond_to do |format|
      format.html
    end
  end

  def edit
    @status = ['pending', 'fraud', 'success']
  end

  def destroy
    @bids.destroy
    respond_to do |format|
      format.html { redirect_to admin_bids_path, notice: 'Order berhasil dihapus.' }
    end
  end

  def update
    respond_to do |format|
      if @bids.update(order_params)
        format.html { redirect_to admin_bids_path, notice: 'Order berhasil diubah.' }
      else
        format.html { render :edit }
      end
    end
  end

  private
    def set_order
      @bids = Order.find(params[:id])
    end
    def order_params
      params.require(:order).permit(
        :status
        )
    end
end
