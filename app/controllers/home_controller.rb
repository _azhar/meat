class HomeController < ApplicationController
  layout "home"

  def index
  end

  def about
  end

  def contact
  end

  def price
    @properties = Property.all
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "reoprts-#{Time.now.strftime('%Y-%m-%d')}",
               orientation: 'Landscape'
      end
    end
  end

  def order
    @order = Order.new
    Property.all.each do |property|
      @order.items.build(property_id: property.id)
    end
  end

  def create_order
    @order = Order.new(order_params)
    respond_to do |format|
      if verify_recaptcha(model: @order) && @order.save
        format.html { redirect_to order_path, notice: 'Order berhasil dibuat.' }
      else
        format.html { render :order }
      end
    end
  end

  private
    def order_params
      params.require(:order).permit(
        :order_time,
        :name,
        :phone_number,
        :address,
        :email,
        items_attributes: [
          :id,
          :total_kg,
          :property_id
          ]
        )
    end
end
