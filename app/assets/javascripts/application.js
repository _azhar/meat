// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.min
//= require jquery_ujs
//= require moment/min/moment.min
//= require turbolinks
//= require chart.min
//= require app
//= require index
//= require bootstrap/dist/js/bootstrap.min
//= require eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min
//= require bootstrap-switch/dist/js/bootstrap-switch.min
//= require matchHeight/jquery.matchHeight-min
//= require DataTables/media/js/jquery.dataTables.min
//= require DataTables/media/js/dataTables.bootstrap.min
//= require select2/dist/js/select2.full.min
//= require ace-builds/src/ace
//= require ace-builds/src/mode-html
//= require ace-builds/src/theme-github
